<?php
/*
Template Name: О компании
*/

get_header(); ?>

	<section class="sidebar-block">
    <div class="logo">
        <a href="<?php echo get_home_url(); ?>"><?php the_field('name_site','option'); ?></a>
        
    </div>
    <div class="list-menu">
        <?php wp_nav_menu( array(
    'theme_location'  => 'menu-1',
    
) ); ?>
<li class="last-interactive-map"><a href="/karta/">Интерактивная карта</a></li>
    </div>
    <div class="social-network">
        <span>Мы в соцсетях</span>
        <div class="social-links">
           <?php if( have_rows('soc', 'option') ): ?>
    <?php while( have_rows('soc', 'option') ): the_row(); 
        ?>
         <a target="_blank" href="<?php the_sub_field('link_soc', 'option')?>" style="background: url('<?php the_sub_field('im_soc', 'option')?>') center no-repeat;"></a>
    <?php endwhile; ?>
<?php endif; ?>
        </div>
    </div>
</section>


<section class="main-content">
    <div class="full-height-content" style="background: url('<?php the_field('image'); ?>') top/cover no-repeat;">
        <div class="opacity-overlay"></div>
        <div class="content-part">
            <div class="main-top-menu">
                 <?php wp_nav_menu( array(
    'theme_location'  => 'menu-2',
) ); ?>

                <!-- div class="callback-block">
                    <a href="#" target="_blank">Обратная связь</a>
                </div>
               <<div class="search">
                    <form action="">
                        <input type="text" placeholder="Поиск по порталу">
                        <input type="submit">
                    </form>
                </div>-->
            </div>
            <div class="wrap-content">
                <div class="about-content">
                    <div class="about-title">
                        <h1><?php the_field('name'); ?></h1>
                        <div class="sub-about-tit">
                           <?php the_field('opis'); ?>
                        </div>
                    </div>
                    <div class="about-slider-content">
                        <div class="overlay-title"><h2>Руководство</h2></div>
                        <div class="top-slider top-slider-top">
                            <?php if( have_rows('ruk') ): ?>
                                <?php while( have_rows('ruk') ): the_row(); ?>
                                    <div class="team-member">
                                        <div class="team-avatar">
                                            <img src="<?php the_sub_field('im_ruk')?>" alt="" class="team-avatar-img">
                                        </div>
                                        <div class="team-text">
                                            <div class="team-name"><?php the_sub_field('fio_ruk')?></div>
                                            <div class="team-position"><?php the_sub_field('dol_ruk')?></div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="bottom-content-main">
                        <div class="overlay-title"><h2>Достижения</h2></div>
                        <?php if( have_rows('dost') ): ?>
    <?php while( have_rows('dost') ): the_row();
        ?>
        <div class="col-4">
                            <div class="achievement">
                                <span target="_blank">
                                    <div class="number-achieve"><?php the_sub_field('kolich_dost')?></div>
                                    <span class="text-achieve">
                                        <?php the_sub_field('opis_dost')?>
                                    </span>
                                    <a class="tj" href="/karta/" target="_blank"><span class="show-on-map">Посмотреть на карте</span></a>
                                </span>
                            </div>
                        </div>
    <?php endwhile; ?>
<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="air-content">
        <div class="bold-title">
            <h2><?php the_field('sec1'); ?></h2>
        </div>
        <div class="under-title blue-one">
            <?php the_field('opis1'); ?>
        </div>


        <div class="number-title">
            <span><?php the_field('name_p1'); ?></span>
        </div>
        <div class="text-bigger">
            <?php the_field('opis_p1'); ?>
        </div>
        <div class="service-items-type">

            <?php if( have_rows('per') ): ?>
    <?php while( have_rows('per') ): the_row(); 
        ?>
         <div class="items-type col-4">
                <div class="it-ty-img">
                    <img src="<?php the_sub_field('im_per')?>" alt="">
                </div>
                <a href="<?php the_sub_field('link_per')?>" target="_blank"><?php the_sub_field('name_per')?></a>
                <span><?php the_sub_field('opis_per')?></span>
            </div>
    <?php endwhile; ?>
<?php endif; ?>
           
        </div>

        <div class="number-title">
            <span><?php the_field('name_p2'); ?></span>
        </div>
        <div class="text-bigger">
           <?php the_field('opis_p2'); ?>
        </div>
        <div class="slider-channels">

             <?php if( have_rows('info') ): ?>
    <?php while( have_rows('info') ): the_row(); 
        ?>
          <div class="item-channel">
                <a href="<?php the_sub_field('link_info')?>" target="_blank">
                    <img src="<?php the_sub_field('im_info')?>" alt="">
                    <div class="text-channel">
                        <div class="info-ch">
                            <?php the_sub_field('name_info')?>
                        </div>
                        <div class="time-ch">
                            <?php the_sub_field('time_info')?>
                        </div>
                    </div>
                </a>
            </div>
    <?php endwhile; ?>
<?php endif; ?>

        </div>


        <div class="number-title">
            <span><?php the_field('name_p3'); ?></span>
        </div>
        <div class="wave-list">
            <ul>
                <?php if( have_rows('opis_3') ): ?>
    <?php while( have_rows('opis_3') ): the_row(); 
        ?>
         <li><?php the_sub_field('opis_text')?></li>
    <?php endwhile; ?>
<?php endif; ?>
            </ul>
        </div>




        <div class="bold-title">
            <h2><?php the_field('sec2'); ?></h2>
        </div>

        <div class="departments-slider">


             <?php if( have_rows('pers') ): ?>
    <?php while( have_rows('pers') ): the_row(); 
        ?>
         <div class="item-heads">
                <div class="avatar-img">
                    <img src="<?php the_sub_field('im_pers')?>" alt="">
                </div>
                <span class="name-avatar"><?php the_sub_field('name_pers')?></span>
                <span class="position-avatar">
                    <?php the_sub_field('dol_pers')?>
                </span>
            </div>
    <?php endwhile; ?>
<?php endif; ?>

        </div>

        <div class="pdf-content blue-bg">
            <div class="text-vacancy">
                <div class="vac-tit">Вакансии</div>
                <div class="sub-tit-vac"><?php the_field('name_vak'); ?> / <span><?php the_field('zarp'); ?></span></div>
                <div class="text-content"><?php the_field('opis_rab'); ?></div>

                <div class="vac-links">
                    <a href="<?php the_field('link_otl'); ?>" target="_blank">Откликнуться на вакансию</a>
                    <a href="<?php the_field('link_all'); ?>" target="_blank">Посмотреть все вакансии</a>
                </div>
            </div>
        </div>

        <div class="bold-title">
            <h2>Как с нами связаться</h2>
        </div>
        <div class="form-contact row">
            <div class="col-6 form-side row">
                <form id="form" action="">
                    <div class="col-6">
                        <div class="form-field text-field">
                            <input name="name" type="text" placeholder="Ваше имя">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-field email-field">
                            <input name="email" type="email" placeholder="Ваш e-mail">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-field select-field">
                            <select name="sel" id="">
                                  <?php if( have_rows('team', 'option') ): ?>
    <?php while( have_rows('team', 'option') ): the_row(); 
        ?>
         <option value=""><?php the_sub_field('name_team', 'option')?></option>
    <?php endwhile; ?>
<?php endif; ?>
                               
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-field textarea-field">
                            <textarea name="text" id=""  placeholder="Текст сообщения"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-field action-field">
                            <input type="submit" value="Отправить">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-6 contact-items">
                <div class="tel-phone">
                    <a><?php the_field('phone','option'); ?></a>
                    <span style="opacity: 0;">Горячая линия Единой справочной службы</span>
                </div>
                <div class="other-contacta">
                    <div class="address-fax">
                        <span>Факс:</span><?php the_field('fax','option'); ?><br>
                        <span>Адрес:</span><?php the_field('adress','option'); ?>
                    </div>
                    <div class="some-text">
                       <?php the_field('dop_info','option'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="row">
                <div class="col-6 right-footer">
                    <div class="footer-tit">Контакты</div>
                    <div class="footer-contacts">
                        <div><span>Тел.:</span><?php the_field('phone','option'); ?></div>
                        <div><span>Факс:</span><?php the_field('fax','option'); ?></div>
                        <div><span>E-mail:</span><?php the_field('e-mail','option'); ?></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="visually-block">
                    
                        <a href="<?php the_field('map','option'); ?>" class="map-site">Карта сайта</a>
                    </div>
                    <div class="copyright">
                       <?php the_field('copy','option'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

	
<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/highchart.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/common.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/slick.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/common.js" type="text/javascript"></script>

<script>
    $('.top-slider-top').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        vertical: true,
        dots: true,
        arrows: false,
//        autoplay: true,
//        autoplaySpeed: 3000
    });


    $('.slider-channels').slick({
        infinite: true,
        slidesToShow:4,
        slidesToScroll: 4,
        dots: true,
        arrows: true,
//        autoplay: true,
//        autoplaySpeed: 3000
    });

    $('.departments-slider').slick({
        infinite: true,
        slidesToShow:3,
        slidesToScroll: 1,
        dots: false,
        arrows: true
//        autoplay: true,
//        autoplaySpeed: 3000
    });

</script>
<?php
get_footer();