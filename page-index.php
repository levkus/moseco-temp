<?php
/*
Template Name: Главная
*/

get_header(); ?>

<?php
    $weather = get_field('home_weather');
?>

<section class="sidebar-block">
    <div class="logo">
        <a href="
        <?php echo get_home_url(); ?>"><?php the_field('name_site','option'); ?></a>

    </div>
    <div class="list-menu">


       <?php wp_nav_menu( array(
           'theme_location'  => 'menu-1',

           ) ); ?>

           <li class="last-interactive-map"><a href="/karta/">Интерактивная карта</a></li>
       </div>
       <div class="social-network">
        <span>Мы в соцсетях</span>
        <div class="social-links">

            <?php if( have_rows('soc', 'option') ): ?>
                <?php while( have_rows('soc', 'option') ): the_row(); 
                ?>
                <a href="<?php the_sub_field('link_soc', 'option')?>" target="_blank" style="background: url('<?php the_sub_field('im_soc', 'option')?>') center no-repeat;"></a>
            <?php endwhile; ?>
        <?php endif; ?>

    </div>
</div>
</section>


<section class="main-content">
    <div class="full-height-content" style="background: url('<?php the_field('sl_image'); ?>') top/cover no-repeat;">
        <div class="opacity-overlay"></div>
        <div class="content-part">
            <div class="main-top-menu">
              <?php wp_nav_menu( array(
               'theme_location'  => 'menu-2',
               ) ); ?>
                <!--<div class="search">
                    <form action="">
                        <input type="text" placeholder="Поиск по порталу">
                        <input type="submit">
                    </form>
                </div>-->
            </div>
            <div class="wrap-content">
                <div class="left-inner-content">
                    <div class="air-info">
<!--                         <div class="air-block">
                            <div class="air-tit">
                               Дальность<br>видимости
                            </div>
                            <div class="air-res">
                                <?php the_field('dal_vid'); ?>
                                <div class="about-result">
                                    <?php the_field('opis1'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="air-block">
                            <div class="air-tit">
                                Текущие условия<br>рассеивания в атмосфере
                            </div>
                            <div class="air-res">
                                <?php if( have_rows('tech_us') ): ?>
    <?php while( have_rows('tech_us') ): the_row(); 
        $image = get_sub_field('day_us');
        $content = get_sub_field('t_us');
        $link = get_sub_field('date_us');
        $link2 = get_sub_field('time_us');
        ?>
        <div class="fr"><?php echo $image ?><br><?php echo $content ?><br><?php echo $link ?><br><?php echo $link2 ?></div>
    <?php endwhile; ?>
<?php endif; ?>
                                <div class="about-result">
                                    <?php the_field('opis2'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="air-block f1">
                            <div class="air-res">
                                <?php the_field('zag_v'); ?>
                                <div class="about-result">
                                  <?php the_field('opis3'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="air-block f1">
                            <div class="air-res">
                                 <?php the_field('shum'); ?>
                                <div class="about-result">
                                   <?php the_field('opis4'); ?>
                                </div>
                            </div>
                        </div> -->





                        <div class="air-container">
                            <div class="air-name">
                                Ветер
                            </div>
                            <div class="air-text">
                                 <?php
$res = $wpdb->get_var("SELECT h_st.modifyav FROM h_st WHERE  h_st.parametername = '| V |' AND h_st.stationname = 'Останкино 0'  AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME)  AND DAYOFMONTH(h_st.time_ok) = DAYOFMONTH(CURRENT_TIME) GROUP BY parametername");
                                ?>
                                 <?php 
                                    if ($res>0){
                                        echo round($res, 1);
                                   ?>
                                    м/с, <?php
$res2 = $wpdb->get_var("SELECT h_st.modifyav FROM h_st WHERE  h_st.parametername = '_V_' AND h_st.stationname = 'Останкино 0'  AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME)  AND DAYOFMONTH(h_st.time_ok) = DAYOFMONTH(CURRENT_TIME) GROUP BY parametername");
                                ?>
                                <?php 
                                    if ($res2>=0 && $res2<45){
                                        echo "с";
                                    } else if($res2>=45 && $res2<45 ){
 echo "св";
                                    }
                                    else if($res2>=45 && $res2<90){
 echo "в";
                                    }
                                    else if($res2>=90 && $res2<135){
 echo "юв";
                                    }
                                    else if($res2>=135 && $res2<180){
 echo "ю";
                                    }
                                    else if($res2>=180 && $res2<225){
 echo "юз";
                                    }
                                    else if($res2>=225 && $res2<270){
 echo "з";
                                    }
                                    else if($res2>=270 && $res2<315){
 echo "сз";
                                    }
                                    else {
 echo "с";
                                    }
                                    ?>

                                    <?php
                                }
else {
 echo "нет";
                                    }
                                    ?>
                            </div>
                        </div>

                        <div class="air-container">
                            <div class="air-name">
                                Давление
                            </div>
                            <div class="air-text">
                                <?php
$res = $wpdb->get_var("SELECT h_st.modifyav FROM h_st WHERE  h_st.parametername = 'Давление' AND h_st.stationname = 'Останкино 0'  AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME)  AND DAYOFMONTH(h_st.time_ok) = DAYOFMONTH(CURRENT_TIME) GROUP BY parametername");
                                ?>
                                <?php echo round($res, 0);
                                    ;
                                    ?> мм рт. ст
                            </div>
                        </div>

                        <div class="air-container">
                            <div class="air-name">
                                Влажность
                            </div>
                            <div class="air-text">
                                 <?php
$res = $wpdb->get_var("SELECT h_st.modifyav FROM h_st WHERE  h_st.parametername = 'Влажность' AND h_st.stationname = 'Останкино 0'  AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME)  AND DAYOFMONTH(h_st.time_ok) = DAYOFMONTH(CURRENT_TIME) GROUP BY parametername");
                                ?>
                                <?php echo round($res, 0);
                                    ;
                                    ?>%
                            </div>
                        </div>

                        <div class="air-container">
                            <div class="air-name">
                                Сейчас, <?php 

$date = date_create($row['theDate']);
date_timezone_set($date, timezone_open('Europe/Moscow'));
echo date_format($date, 'H:i') . "\n";

                 ?> 
                            </div>
                            <div class="air-text">
                                Дождь со снегом
                            </div>
                        </div>






















                    </div>
                    <div class="big-title">
                        <h1><?php the_field('n_p'); ?></h1>
                        <div class="big-title-city">города Москвы</div>
                    </div>
                    <div class="home-eco-now">
                        <span class="home-grad-header">Экологическая обстановка</span>
                        <button class="home-more-link">Подробнее</button>
                    </div>
                    <div class="home-eco-preview">
                        <?php echo mb_strimwidth($weather['text'], 0, 120, '...'); ?>
                    </div>

            <div class="slider-home">

              <?php
global $post;
$args = array( 'category' => 2, 'orderby' => 'date');
$myposts = get_posts( $args );
foreach( $myposts as $post ){ setup_postdata($post);
    $thetitle = $post->post_title;
?>

        <div class="slider-home-item">
            <div class="home-item-wrap"  style="background-image: url(<?php the_field('img'); ?>);">
                 <div class="home-item-inner">
                    <?php the_category(', ') ?>
                    <div><?php echo mb_strimwidth($thetitle, 0, 50, '...'); ?></div>
                </div>
             </div>
             <span class="slider-home-data">
                <span class="slider-home-date"><?php the_date( 'j F Y'); ?></span>
                <?php the_time(); ?>
            </span>
        </div>
<?php
}
wp_reset_postdata();
?>
             


              


</div>










</div>
<div class="right-inner-content">
    <div class="weather-today">

        <!--<?php echo do_shortcode('[wcp_weather id="wcp_openweather_5a6a5e93f05a9" city="Москва, Россия" city_data="city=Москва&country=Россия&full_name=Москва, Россия&lat=55.755826&lng=37.617299900000035" tempUnit="c" windSpeedUnit="ms" pressureUnit="mmHg" template="default" showCurrentWeather="on" showForecastWeather=""]'); ?>-->

    </div>
    
    <div class="right-news_item">



        <div class="news-temp">
           <!-- <img src="wp-content/themes/moseco/img/Stroke-15.png" alt="">-->
             <?php
$res = $wpdb->get_var("SELECT h_st.modifyav FROM h_st WHERE  h_st.parametername = '-T-' AND h_st.stationname = 'Останкино 0'  AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME)  AND DAYOFMONTH(h_st.time_ok) = DAYOFMONTH(CURRENT_TIME) GROUP BY parametername");
                                ?>
                               
            <span><?php echo round($res, 0);
                                    ;
                                    ?>°</span>
        </div>

    <div class="right-news_item">
        <?php the_field('k_v_j'); ?>
    </div>

    </div>

    <div class="right-news_item">
        <?php the_field('in_zg'); ?>
    </div>

    <div class="right-news_item">
        <?php the_field('voda'); ?>
    </div>












<!-- <div class="param-slider">
<div class="slider-item">
                    <div class="inf-weather">
                        <div class=""><span><?php the_field('sost_atm1'); ?></span>
                        <div class="number-weather">2/5</div>
                        <div class="number-weather"><span><?php echo date("d.m.Y"); ?></span></div></div>

                    </div>
                      </div>
   <div class="slider-item">
                    <div class="inf-weather">

                        <div class=""><span><?php the_field('sost_atm2'); ?></span>
                        <div class="number-weather">3/5</div>
                        <div class="number-weather"><span><?php echo date("d.m.Y"); ?></span></div></div>
                    </div>
                    </div>
                </div> -->
<!--                     <div class="inf-weather">
 <?php the_field('in_zag'); ?>
</div> -->
<!--                    <div class="param-slider">
<div class="slider-item">
                    <div class="inf-weather">
                         <div class=""><?php the_field('con_ok1'); ?></div>
                
                    </div>
                </div>
<div class="slider-item">

                    <div class="inf-weather">
                         <div class=""><?php the_field('con_ok2'); ?></div>
                    </div>
                 </div>
                <div class="slider-item">
 <div class="inf-weather">
                         <div class=""><?php the_field('con_ok3'); ?></div>
                    </div>
                   
                     </div>

                   
                 </div> -->

      <!--               <div class="inf-weather">
                       <?php the_field('prob_vod'); ?>
                    </div>

                -->

<!--                     <div class="inf-weather">
                       <?php the_field('prob_vod'); ?>
                    </div>
                -->

 <!--                    <div class="inter-map">
                        <a href="http://moseco.y-web.by/karta/">
                            <img src="<?php echo get_template_directory_uri();?>/img/intermap.png" alt="">
                            <span>Интерактивная<br>карта</span>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<div class="home-modal">
    <div class="home-modal-inner">
        <div class="home-modal-content">
            <div class="home-modal-title">
                <img class="home-modal-gerb" src="<?php echo get_template_directory_uri();?>/img/gerb.png" alt="">
                <span class="home-modal-title-text">Правительство Москвы</span>
            </div>
            <div class="home-modal-header"><?php echo $weather['weather-header'] ?></div>
            <div class="home-modal-date">Экологическая обстановка в Москве на <?php echo $weather['time'] ?> <?php echo $weather['date'] ?></div>
            <div class="home-modal-text"><?php echo $weather['text'] ?></div>
            <hr class="home-modal-hr">
            <div class="home-modal-small-text"><?php echo $weather['small-text'] ?></div>
            <!-- <div class="pdf-downloads">
                <?php if( have_rows('doc') ): ?>
                    <?php while( have_rows('doc') ): the_row(); 
                        $image = get_sub_field('name_doc');
                        $content = get_sub_field('item_doc');
                        $link = get_sub_field('time_doc');
                    ?>
                        <div class="pdf-block">
                            <a href="<?php echo $content; ?>" target="_blank" class="down-pdf"></a>
                            <div class="text-self">
                                <span><?php echo $image; ?><span class="overlay-fon">состояние на <?php echo $link; ?></span></span>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div> -->
        </div>
        <button class="home-modal-close"></button>
    </div>
</div>


<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/highchart.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/slick.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/common.js" type="text/javascript"></script>

<script>
    $('.home-modal').on('click', function (e) {
        $(this).fadeOut()
    })

    $('.home-modal-close').on('click', function () {
        $('.home-modal').fadeOut()
    })

    $('.home-modal-inner').on('click', function (e) {
        e.stopPropagation()
        e.preventDefault()
    })

    $('.home-more-link').on('click', function () {
        $('.home-modal').fadeIn()
    })

    $('.param-slider').slick({
        slidesToShow:1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        vertical: true
        // autoplay: true,
        // autoplaySpeed: 3000
    });



    $('.slider-home').slick({
        infinite: true,
        slidesToShow:2,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
//        autoplay: true,
//        autoplaySpeed: 3000
});



</script>

<?php
get_footer();