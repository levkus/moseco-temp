<?php
/*
Template Name: Воздух
*/

get_header(); ?>

<section class="sidebar-block">
    <div class="logo">
        <a href="
<?php echo get_home_url(); ?>"><?php the_field('name_site','option'); ?></a>
    
    </div>
    <div class="list-menu">
       <!-- <div class="consist-air">
            <span>Состояние окружающей <br> среды: <span class="red-color">воздух</span></span>
            <ul>
                <li class="active">Состояние сегодня</li>
                <li>Загрязнение воздуха</li>
                <li>Система мониторнга воздуха</li>
            </ul>
        </div>-->

         <?php wp_nav_menu( array(
    'theme_location'  => 'menu-1',
    
) ); ?>
<li class="last-interactive-map"><a href="/karta/">Интерактивная карта</a></li>
       
    </div>
    <div class="social-network">
        <span>Мы в соцсетях</span>
        <div class="social-links">

            <?php if( have_rows('soc', 'option') ): ?>
    <?php while( have_rows('soc', 'option') ): the_row(); 
        ?>
         <a href="<?php the_sub_field('link_soc', 'option')?>" target="_blank" style="background: url('<?php the_sub_field('im_soc', 'option')?>') center no-repeat;"></a>
    <?php endwhile; ?>
<?php endif; ?>
          
        </div>
    </div>
</section>


<section class="main-content">
    <div class="full-height-content" style="background: url('<?php the_field('sl_image'); ?>') top/cover no-repeat;">
        <div class="opacity-overlay"></div>
        <div class="inter-map">
            <a href="/karta/">
                <img src="<?php echo get_template_directory_uri();?>/img/intermap.png" alt="">
                <span>Интерактивная<br>карта</span>
            </a>
        </div>
        <div class="content-part">
            <div class="main-top-menu">
                 <?php wp_nav_menu( array(
    'theme_location'  => 'menu-2',
) ); ?>
               <!-- <div class="search">
                    <form action="">
                        <input type="text" placeholder="Поиск по порталу">
                        <input type="submit">
                    </form>
                </div>-->
            </div>
            <div class="wrap-content">
                <div class="left-inner-content">
                    <div class="air-info">
                        <div class="air-block">
                            <div class="air-tit">
                                Индекс загрязнения<br>атмосферы
                            </div>
                            <div class="air-res">
                                <?php the_field('zag_vod'); ?>
                                <div class="about-result">
                                    <?php the_field('op2'); ?><br>
                                </div>
                            </div>
                        </div>
                        <div class="air-block">
                            <div class="air-tit">
                                Индекс загрязнения<br>воздуха (годовой)
                            </div>
                            <div class="air-res">
                                <?php the_field('neft'); ?>
                                <div class="about-result">
                                    ПДК - <?php the_field('op3'); ?><br>
                                </div>
                            </div>
                        </div>
                        <div class="air-block">
                            <div class="air-tit">
                                Условия рассеивания
                            </div>
                            <div class="air-res">
                                <?php the_field('shum'); ?>
                                <div class="about-result">
                                   <?php the_field('op4'); ?><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="big-title">
                        <h1><?php the_field('n_p'); ?></h1>
                        <span>города Москвы</span>
                    </div>
                    <div class="pdf-downloads">
                        <?php if( have_rows('doc') ): ?>
    <?php while( have_rows('doc') ): the_row(); 
        $image = get_sub_field('name_doc');
        $content = get_sub_field('item_doc');
        $link = get_sub_field('time_doc');
        ?>
<div class="pdf-block">
                            <a href="<?php echo $content; ?>" target="_blank" class="down-pdf"></a>
                            <div class="text-self">
                                <span><?php echo $image; ?><span class="overlay-fon">по состоянию на <?php echo $link; ?></span></span>
                            </div>
                        </div>
    <?php endwhile; ?>
<?php endif; ?>
                    </div>
                    
                 <!--   <div class="articles-content">

                    <?php
global $post;
$args = array( 'numberposts' => 2 , 'category' => 2, 'orderby' => 'date');
$myposts = get_posts( $args );
foreach( $myposts as $post ){ setup_postdata($post);
?>

                    <div class="article-item">
                            <div class="article-img">
                                <a href="#" target="_blank">
                                    <img src="<?php the_field('img'); ?>">
                                    <div class="text-article">
                                        <div class="category"><?php the_category(', ') ?></div>
                                        <div class="short-text"><?php the_title(); ?></div>
                                    </div>
                                </a>
                            </div>
                           
                        </div>
<?php
}
wp_reset_postdata();
?>
                      
                        
                    </div>-->
                </div>
                <div class="right-inner-content">
                  <!--  <div class="weather-today">
                       <?php echo do_shortcode('[wcp_weather id="wcp_openweather_5a6a5e93f05a9" city="Москва, Россия" city_data="city=Москва&country=Россия&full_name=Москва, Россия&lat=55.755826&lng=37.617299900000035" tempUnit="c" windSpeedUnit="ms" pressureUnit="mmHg" template="default" showCurrentWeather="on" showForecastWeather=""]'); ?>-->
                    </div>
                    <div class="inf-weather">
                        <div class="number-weather"><?php the_field('sr_god'); ?></div>
                        <span>Автоматических станций контроля загрязнения воздуха</span>
                        <a href="/karta/" target="_blank"><span class="show-on-map" target="_blank">Посмотреть на карте</span></a>
                    </div>
                    <div class="inf-weather">
                        <div class="number-weather"><?php the_field('park'); ?></div>
                        <span>Анализируемых загрязняющих веществ</span>
                        <a href="/karta/" target="_blank"><span class="show-on-map" target="_blank">Посмотреть на карте</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="air-content">
        <div class="bold-title">
            <h2>Текущее состояние воздуха в городе</h2>
        </div>
        <div class="under-title">
            Качество воздуха в городе круглосуточно контролируется автоматическими станциями «Мосэкомониторинга»
        </div>
        <div class="text-content">
           <?php the_field('sost'); ?>
        </div>
        <div class="rectangle-blocks">
            <div class="title-time">
               
                                    

                <h3>Состояние на <?php 

$date = date_create($row['theDate']);
date_timezone_set($date, timezone_open('Europe/Moscow'));
echo date_format($date, 'H:i') . "\n";

                 ?> 
                <?php 
                date_timezone_set('Europe/Moscow');    
                echo date("d.m.Y"); ?></h3>
                изменение к вчерашнему дню, мкг/м3
                <div class="tabs-red-line">
                    <span class="active">В жилых районах</span>
                    <span >Вдоль автотрасс</span>
                </div>

            </div>
           <div class="clear-content  content-for-tab" >
     
            <div class="content-tab active">
                <?php 

global $wpdb;
$res = $wpdb->get_results("SELECT h_st.parametername, AVG(h_st.modifyav),h_st.time_ok, wp_parameters.norma, wp_parameters.opis, wp_parameters.otobr, wp_parameters.num, wp_parameters.name, wp_parameters.class  FROM h_st, wp_parameters WHERE  h_st.parametername = wp_parameters.parametername AND wp_parameters.otobr = 'Отображать' AND HOUR(h_st.time_ok) = HOUR(CURRENT_TIME) GROUP BY parametername", ARRAY_A);

//а затем обычным циклом выводишь
$result = "";
foreach($res as $key)
{

  ?>
    
               <div class="item-block m-flip lm4">
                        <div class="m-flip__content">
                            <div class="front">
                                <div class="norma">
                                    <div class="text-norma"><?php 
                                     echo $key['parametername'];
                                    ?></div>
                                    <div class="info-icon"></div>
                                </div>
                                <div class="center-mode">
                                    <div class="item-type">
                                        <span class="first-type"><?php 
                                     echo $key['name'];
                                    ?></span>
                                        <span class="total-type">
                                            <span class="nuqta yellow"></span>
                                            <span class="this-count"><?php echo round($key['AVG(h_st.modifyav)'], 3);
                                    ;
                                    ?></span>
                                            <span class="extrimum up"><?php
                                $t = $key['AVG(d_st.modifyav)'] - $key['AVG(h_st.modifyav)'];
                                 echo round($t, 3);
                                    ;
                                    ?></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="item-bottom">
                                    <div class="normasi">
                                        Норма: до <?php 
                                      echo round($key['norma'], 3);
                                    ?> мкг/м3
                                    </div>
                                    <div class="class-safety">
                                        Класс опасности: <span class="total-safety"><?php 
                                     echo $key['class'];
                                    ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="back">
                                <div class="close-back">x</div>
                                <div class="back-title"><?php 
                                     echo $key['name'];
                                    ?></div>
                                <div class="back-text"><?php 
                                     echo $key['opis'];
                                    ?></div>
                            </div>
                        </div>
                    </div>

<?php 
     }


?>
   <div class="load-more">
                <a href="#" id="loadMore4">Загрузить еще</a>
            </div> 
            <script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
            <script>

                $(document).ready(function(){
          $(".lm4").slice(0, 4).show();
    $("#loadMore4").on('click', function (e) {
        e.preventDefault();
        $(".lm4:hidden").slice(0, 4).slideDown();
        if ($(".lm4:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
       
    });
    })
               
            </script>         
</div>

   

<div class="content-tab">
                  <?php 

global $wpdb;
$res2 = $wpdb->get_results("SELECT h_st_a.parametername, AVG(h_st_a.modifyav),h_st_a.time_ok, wp_parameters.norma, wp_parameters.opis, wp_parameters.otobr, wp_parameters.num, wp_parameters.name, wp_parameters.class  FROM h_st_a, wp_parameters WHERE  h_st_a.parametername = wp_parameters.parametername AND wp_parameters.otobr = 'Отображать' AND HOUR(h_st_a.time_ok) = HOUR(CURRENT_TIME) GROUP BY parametername", ARRAY_A);

//а затем обычным циклом выводишь
$result2 = "";
foreach($res2 as $key2)
{

  ?>
    
               <div class="item-block m-flip lm5">
                        <div class="m-flip__content">
                            <div class="front">
                                <div class="norma">
                                    <div class="text-norma"><?php 
                                     echo $key2['parametername'];
                                    ?></div>
                                    <div class="info-icon"></div>
                                </div>
                                <div class="center-mode">
                                    <div class="item-type">
                                        <span class="first-type"><?php 
                                     echo $key2['name'];
                                    ?></span>
                                        <span class="total-type">
                                            <span class="nuqta yellow"></span>
                                            <span class="this-count"><?php echo round($key2['AVG(h_st_a.modifyav)'], 3);
                                    ;
                                    ?></span>
                                            <span class="extrimum up"><?php
                                $t = $key2['AVG(d_st_a.modifyav)'] - $key2['AVG(h_st_a.modifyav)'];
                                 echo round($t, 3);
                                    ;
                                    ?></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="item-bottom">
                                    <div class="normasi">
                                        Норма: до <?php 
                                      echo round($key2['norma'], 3);
                                    ?> мкг/м3
                                    </div>
                                    <div class="class-safety">
                                        Класс опасности: <span class="total-safety"><?php 
                                     echo $key2['class'];
                                    ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="back">
                                <div class="close-back">x</div>
                                <div class="back-title"><?php 
                                     echo $key2['name'];
                                    ?></div>
                                <div class="back-text"><?php 
                                     echo $key2['opis'];
                                    ?></div>
                            </div>
                        </div>
                    </div>

<?php 
     }


?>

<div class="load-more">
                <a href="#" id="loadMore5">Загрузить еще</a>
            </div>
              <script>

                $(document).ready(function(){
          $(".lm5").slice(0, 4).show();
    $("#loadMore5").on('click', function (e) {
        e.preventDefault();
        $(".lm5:hidden").slice(0, 4).slideDown();
        if ($(".lm5:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
       
    });
    })
               
            </script> 
</div>
</div>
            
        </div>

        <!--div class="bold-title">
            <h2>Загрязнение воздуха</h2>
        </div>
        <div class="with-mos-icon">
            <div class="text-content">
                <?php the_field('zagr'); ?>
            </div>
        </div-->
        <div class="chart-content">

            <div class="content-air">
                <div class="air-title">
                    <h2>Содержание в воздухе, <span>мкг/м3</span></h2>
                    <div class="tt tabs-red-line">
                    <span class="active">В жилых районах</span>
                    <span >Вдоль автотрасс</span>
                </div>
                </div>
              

            </div>

               <div class="tt content-for-tab" >
                <div class="chart-tabs w3-bar w3-black tt content-tab active">
<div class="ty">

                      <?php 

                 $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link3=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link3);

$query3 ="SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result3=mysql_query($query3);

while ($row3 = mysql_fetch_assoc($result3)) { 
     $rows3[] = $row3; 
} 

     //do something with $rows[$i]['field_name'] 
for($i3=0;$i3<1;$i3++)
{
  ?>
<span class="w3-bar-item2 active w3-button" onclick="openCity('<?php echo $rows3[$i3]['id']?>')"><?php echo $rows3[$i3]['parametername']?></span>
<?php 
}
for($i3=1;$i3<count($rows3);$i3++)
{
  ?>
<span class="w3-bar-item2 w3-button" onclick="openCity('<?php echo $rows3[$i3]['id']?>')"><?php echo $rows3[$i3]['parametername']?></span>
<?php 
}
   mysql_free_result($result3);

mysql_close($link3);



 

?>
  </div>               
 <?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link);

$query4 ="SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result4=mysql_query($query4);

while ($row4 = mysql_fetch_assoc($result4)) { 
     $rows4[] = $row4; 
} 

     //do something with $rows[$i]['field_name'] 

for($i4=0;$i4<1;$i4++)
{
  ?>

 <div id="<?php echo $rows4[$i4]['id']?>" class="city12" >

<div  id="d<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122">
 <div id="container-chart<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

</div>

<div id="n<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
<div id="container-chartn<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div id="m<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
<div id="container-chartm<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>


<div id="g<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
 <div id="container-charty<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

            <div class="tab-date">
                <span class="w3-bar-item222 active w3-button" id="d2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('d<?php echo $rows4[$i4]['id']?>')">День</span>
                <span class="w3-bar-item222 w3-button" id="w2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('n<?php echo $rows4[$i4]['id']?>')">Неделя</span>
        <span class="w3-bar-item222 w3-button" id="m2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('m<?php echo $rows4[$i4]['id']?>')">Месяц</span>
                <span class="w3-bar-item222 w3-button" id="y2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('g<?php echo $rows4[$i4]['id']?>')">Год</span>
            </div>
</div>

<?php 
}


for($i4=1;$i4<count($rows4);$i4++)
{
  ?>

 <div id="<?php echo $rows4[$i4]['id']?>" class="city12" style="display:none">
<div  id="d<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122">
 <div id="container-chart<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

</div>

<div id="n<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
<div id="container-chartn<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div id="m<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
<div id="container-chartm<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>


<div id="g<?php echo $rows4[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>122" style="display:none">
 <div id="container-charty<?php echo $rows4[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

            <div class="tab-date">
                <span class="w3-bar-item active w3-button" id="d2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('d<?php echo $rows4[$i4]['id']?>')">День</span>
                <span class="w3-bar-item w3-button" id="w2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('n<?php echo $rows4[$i4]['id']?>')">Неделя</span>
                <span class="w3-bar-item w3-button" id="m2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('m<?php echo $rows4[$i4]['id']?>')">Месяц</span>
              
                <span class="w3-bar-item w3-button" id="y2<?php echo $rows4[$i4]['id']?>" onclick="openCity22<?php echo $rows4[$i4]['id']?>('g<?php echo $rows4[$i4]['id']?>')">Год</span>
            </div>
</div>

<?php 
}
   mysql_free_result($result4);

mysql_close($link4);

?>                  
                   
                </div>



                 <div class="chart-tabs voi w3-bar w3-black tt content-tab">
<div class="ty">

                      <?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link311=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link311);

$query311 ="SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result311=mysql_query($query311);

while ($row311 = mysql_fetch_assoc($result311)) { 
     $rows311[] = $row311; 
} 

     //do something with $rows[$i]['field_name'] 
for($i3=0;$i3<1;$i3++)
{
  ?>
<span class="w3-bar-item3 active w3-button" onclick="openCity11('<?php echo $rows311[$i3]['id']?>1')"><?php echo $rows3[$i3]['parametername']?></span>
<?php 
}
for($i3=1;$i3<count($rows3);$i3++)
{
  ?>
<span class="w3-bar-item3 w3-button" onclick="openCity11('<?php echo $rows311[$i3]['id']?>1')"><?php echo $rows3[$i3]['parametername']?></span>
<?php 
}
   mysql_free_result($result311);

mysql_close($link311);



 

?>
                 
     </div>  
 <?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link11=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link11);

$query411 ="SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result411=mysql_query($query411);

while ($row411 = mysql_fetch_assoc($result411)) { 
     $rows411[] = $row411; 
} 

     //do something with $rows[$i]['field_name'] 

for($i4=0;$i4<1;$i4++)
{
  ?>
 <div id="<?php echo $rows411[$i4]['id']?>1" class="city11">


<div  id="d11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122 defaultOpen">
 <div id="container-chart11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

</div>

<div id="n11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
<div id="container-chartn11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div id="m11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
<div id="container-chartm11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>


<div id="g11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
 <div id="container-charty11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

            <div class="tab-date vv">
                <span class="w3-bar-item333 active w3-button" id="d3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('d11<?php echo $rows411[$i4]['id']?>')">День</span>
                <span class="w3-bar-item333 w3-button" id="w3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('n11<?php echo $rows411[$i4]['id']?>')">Неделя</span>
        <span class="w3-bar-item222 w3-button" id="m3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('m11<?php echo $rows411[$i4]['id']?>')">Месяц</span>
                <span class="w3-bar-item333 w3-button" id="y3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('g11<?php echo $rows411[$i4]['id']?>')">Год</span>
            </div>
</div>

<?php 
}


for($i4=1;$i4<count($rows4);$i4++)
{
  ?>

 <div id="<?php echo $rows411[$i4]['id']?>1" class="city11" style="display:none">
<div  id="d11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122">
 <div id="container-chart11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

</div>

<div id="n11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
<div id="container-chartn11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div id="m11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
<div id="container-chartm11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>


<div id="g11<?php echo $rows411[$i4]['id']?>" class="city<?php echo $rows4[$i4]['id']?>1122" style="display:none">
 <div id="container-charty11<?php echo $rows411[$i4]['id']?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

            <div class="tab-date vv">
                <span class="w3-bar-item333 active w3-button" id="d3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('d11<?php echo $rows411[$i4]['id']?>')">День</span>
                <span class="w3-bar-item333 w3-button" id="w3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('n11<?php echo $rows411[$i4]['id']?>')">Неделя</span>
                <span class="w3-bar-item333 w3-button" id="m3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('m11<?php echo $rows411[$i4]['id']?>')">Месяц</span>
              
                <span class="w3-bar-item333 w3-button" id="y3<?php echo $rows4[$i4]['id']?>" onclick="openCity<?php echo $rows4[$i4]['id']?>11222('g11<?php echo $rows411[$i4]['id']?>')">Год</span>
            </div>
</div>

<?php 
}
   mysql_free_result($result411);

mysql_close($link411);

?>            
                   
                </div>

</div>
        
        </div>

        <div class="pdf-content">
            <div class="text-pdf">
                 <?php the_field('rez'); ?>
            </div>
            <a href="<?php the_field('files'); ?>" target="_blank">Скачать</a>
        </div>
        <div class="bold-title">
            <h2>Система мониторинга воздуха</h2>
        </div>
        <div class="text-monitoring">
           <?php the_field('syst'); ?>
        </div>
        <div class="two-columns">
            <?php if( have_rows('monit') ): ?>
    <?php while( have_rows('monit') ): the_row(); 
        $image = get_sub_field('kol');
        $content = get_sub_field('opis');
        ?>
         <div class="system-block">
                <span class="num-symbol"><?php echo $image; ?></span>
                <span class="system-text"><?php echo $content; ?></span>
            </div>
    <?php endwhile; ?>
<?php endif; ?>
           
        </div>
        <div class="avatar-title">Мы отслеживаем:</div>
        <div class="four-columns">
                <?php if( have_rows('otsled') ): ?>
    <?php while( have_rows('otsled') ): the_row(); 
        $image = get_sub_field('kolich');
        $content = get_sub_field('imm');
        $link = get_sub_field('name');
        ?>
         <div class="col-avatar">
                <div class="avatar-img">
                    <img src="<?php echo $content; ?>" alt="">
                </div>
                <div class="text-avatar">
                    <div class="total-count"><?php echo $image; ?></div>
                    <span><?php echo $link; ?></span>
                </div>
            </div>
    <?php endwhile; ?>
<?php endif; ?>
           
        </div>
        <div class="load-more red-bottom">
            <a href="/karta/" target="_blank">Посмотреть на карте </a>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-6 right-footer">
                    <div class="footer-tit">Контакты</div>
                    <div class="footer-contacts">
                        <div><span>Тел.:</span><?php the_field('phone','option'); ?></div>
                        <div><span>Факс:</span><?php the_field('fax','option'); ?></div>
                        <div><span>E-mail:</span><?php the_field('e-mail','option'); ?></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="visually-block">
                       
                        <a href="<?php the_field('map','option'); ?>" class="map-site">Карта сайта</a>
                    </div>
                    <div class="copyright">
                       <?php the_field('copy','option'); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
    
<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/highchart.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/common.js" type="text/javascript"></script>

<style>
    .content-tab {
        display: none;
    }
    .content-tab.active {
        display: block;
    }
</style>



<script>
    $('.tabs-red-line > span').click(function () {
        var tabs = $(this).index();
        var tabItem = $('.content-for-tab > .content-tab');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.content-for-tab').fadeOut(300, function () {
            tabItem.removeClass('active');
            tabItem.eq(tabs).addClass('active');
            $(this).fadeIn(300);
        });
    });
</script>

<script>
    $('.tt.tabs-red-line > span').click(function () {
        var tabs = $(this).index();
        var tabItem = $('.tt.content-for-tab > .tt.content-tab');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.tt.content-for-tab').fadeOut(300, function () {
            tabItem.removeClass('active');
            tabItem.eq(tabs).addClass('active');
            $(this).fadeIn(300);
        });
    });
</script>

<script>
    $('.tt.tabs-red-line > span').click(function () {
        var tabs = $(this).index();
        var tabItem = $('.tt2.content-for-tab > .tt2.content-tab');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.tt2.content-for-tab').fadeOut(300, function () {
            tabItem.removeClass('active');
            tabItem.eq(tabs).addClass('active');
            $(this).fadeIn(300);
        });
    });
</script>


<script>
    $('.ty > span').click(function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
      
    });

    $('.tab-date > span').click(function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
      
    });
</script>



<script>
    $('.voi .w3-bar.w3-black > span').click(function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
      
    });

    $('.vv > span').click(function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
      
    });
</script>
<script>
    $(document).ready(function() {
     document.getElementById("defaultOpen").click();
     document.getElementById("248868").click();
     
});
   
</script>
<script>
	$(document).ready(function() {
     document.getElementByClassName("w3-bar-item2 active").click();
     
});
	
</script>
<script>

$(document).ready(function() {
     document.getElementById("d<?php echo $s?>").click();
     
});




  </script>
 
 <?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";

$link=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link);

$query5 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result5=mysql_query($query5);

while ($row5 = mysql_fetch_assoc($result5)) { 
    $rows5[] = $row5;

} 
 
for($y5=0;$y5<count($rows5);$y5++)
{
$tam = $rows5[$y5]['parametername'] ;
$s = $rows5[$y5]['id'] ;

?>


<?php 
 

$query6 ="SELECT modifyav, parametername, time_ok, date_ok FROM h_par";
$result6=mysql_query($query6);

while ($row6 = mysql_fetch_array($result6)) { 

    if ($rows5[$y5]['parametername'] == $row6['parametername']){
   
        $k1 = $row6['date_ok'];
        $k2 = $row6['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row6['modifyav'];
    $rows6[] = "[$d,$data2 ]";

      }
 }

 
         ?>

    <script>

$(document).ready(function() {
     document.getElementById("d<?php echo $s?>").click();
     
});

function openCity22<?php echo $s?>(cityName) {
    var i;
    var x = document.getElementsByClassName("city<?php echo $s?>122");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    document.getElementById(cityName).style.display = "block"; 
    $(this).addClass('active').siblings().removeClass('active');
 
}



 function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("city12");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    document.getElementById(cityName).style.display = "block"; 

  $(this).addClass('active').siblings().removeClass('active');

};

function openCity<?php echo $s?>112(cityName) {
    var i;
    var x = document.getElementsByClassName("city<?php echo $s?>112");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    document.getElementById(cityName<?php echo $s?>).style.display = "block"; 

     document.getElementById(cityName).style.display = "block"; 

  $(this).addClass('active').siblings().removeClass('active');
 
}




  </script>     

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa']
    });
    Highcharts.chart('container-chart<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
        
 xAxis: {
                type: 'datetime',
                tickInterval: 3600 * 1000,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}",
            xDateFormat: 'Дата: %Y.%l.%d, %H:%M'
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows6, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result6);

$rows6="";
$data="";
$data1="";
$data6="";
}

  mysql_free_result($result5);

   
 

mysql_close($link5);



 

?>


 <?php 

               $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link52=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link52);

$query52 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result52=mysql_query($query52);

while ($row52 = mysql_fetch_assoc($result52)) { 
    $rows52[] = $row52;
} 
 
for($y52=0;$y52<count($rows52);$y52++)
{
$tam = $rows52[$y52]['parametername'] ;
$s = $rows52[$y52]['id'] ;

?>


<?php 
 

$query62 ="SELECT modifyav, parametername, date_ok, time_ok FROM w_par";
$result62=mysql_query($query62);

while ($row62 = mysql_fetch_array($result62)) { 

    if ($rows52[$y52]['parametername'] == $row62['parametername']){
       $k1 = $row62['date_ok'];
        $k2 = $row62['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row62['modifyav'];
    $rows62[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ],

        shortMonths: ['Янв.', 'Фев.', 'Мрт.', 'Апр.', 'Май.', 'Июн.', 'Июл.', 'Авг.', 'Сен.', 'Окт.', 'Нбр.', 'Дек.'],
        
    }
    });
    Highcharts.chart('container-chartn<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
       xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%A', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
    

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows62, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result62);
$data2="";
$rows62="";

}

  mysql_free_result($result52);

   
 

mysql_close($link52);



 

?>



<?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link54=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link54);

$query54 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result54=mysql_query($query54);

while ($row54 = mysql_fetch_assoc($result54)) { 
    $rows54[] = $row54;
} 
 
for($y54=0;$y54<count($rows54);$y54++)
{
$tam = $rows54[$y54]['parametername'] ;
$s = $rows54[$y54]['id'] ;

?>


<?php 
 

$query64 ="SELECT modifyav, parametername, date_ok, time_ok FROM y_par";
$result64=mysql_query($query64);

while ($row64 = mysql_fetch_array($result64)) { 

    if ($rows54[$y54]['parametername'] == $row64['parametername']){
        $k1 = $row64['date_ok'];
        $k2 = $row64['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row64['modifyav'];
    $rows64[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]
    }
    });
    Highcharts.chart('container-charty<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
       xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%B', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows64, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result64);

$rows64="";
$rows2="";
}

  mysql_free_result($result54);

   
 

mysql_close($link54);


?>



<?php 

                 $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link77=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link77);

$query77 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result77=mysql_query($query77);

while ($row77 = mysql_fetch_assoc($result77)) { 
    $rows77[] = $row77;
} 
 
for($y77=0;$y77<count($rows77);$y77++)
{
$tam = $rows77[$y77]['parametername'] ;
$s = $rows77[$y77]['id'] ;

?>


<?php 
 

$query78 ="SELECT modifyav, parametername, date_ok, time_ok FROM m_par";
$result78=mysql_query($query78);

while ($row78 = mysql_fetch_array($result78)) { 

    if ($rows77[$y77]['parametername'] == $row78['parametername']){
     $k1 = $row78['date_ok'];
        $k2 = $row78['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row78['modifyav'];
    $rows78[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]
    }
    });
    Highcharts.chart('container-chartm<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
        xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%d.%m', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },
        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows78, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result78);

$rows78="";
$data2="";
}

  mysql_free_result($result77);

   
 

mysql_close($link77);


?>









<!-- вдоль автотрасы -->


<?php 

                $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link);

$query511 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result511=mysql_query($query511);

while ($row511 = mysql_fetch_assoc($result511)) { 
    $rows511[] = $row511;
} 
 
for($y511=0;$y511<count($rows511);$y511++)
{
$tam = $rows511[$y511]['parametername'] ;
$s = $rows511[$y511]['id'] ;

?>

  <script>
      function openCity11(cityName<?php echo $s?>1) {
    var i;
    var x = document.getElementsByClassName("city11");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    document.getElementById(cityName<?php echo $s?>1).style.display = "block"; 
  $(this).addClass('active').siblings().removeClass('active');
};


function openCity<?php echo $s?>11222(cityName) {
    var i;
    var x = document.getElementsByClassName("city<?php echo $s?>1122");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    

     document.getElementById(cityName).style.display = "block"; 

  $(this).addClass('active').siblings().removeClass('active');
 
}

  </script>
<?php 
 

$query611 ="SELECT modifyav, parametername, date_ok, time_ok FROM h_par_a";
$result611=mysql_query($query611);

while ($row611 = mysql_fetch_array($result611)) { 

    if ($rows511[$y511]['parametername'] == $row611['parametername']){
     $k1 = $row611['date_ok'];
        $k2 = $row611['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row611['modifyav'];
    $rows611[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]
    }
    });
    Highcharts.chart('container-chart11<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
       xAxis: {
                type: 'datetime',
                tickInterval: 3600 * 1000,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}",
            xDateFormat: 'Дата: %Y.%l.%d, %H:%M'
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows611, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result611);

$rows611="";
$data2="";
}

  mysql_free_result($result511);

   
 

mysql_close($link511);



 

?>


 <?php 

                 $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link5211=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link5211);

$query5211 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result5211=mysql_query($query5211);

while ($row5211 = mysql_fetch_assoc($result5211)) { 
    $rows5211[] = $row5211;

} 
 
for($y5211=0;$y5211<count($rows5211);$y5211++)
{
$tam = $rows5211[$y5211]['parametername'] ;
$s = $rows5211[$y5211]['id'] ;

?>

 
<?php 
 

$query6211 ="SELECT modifyav, parametername, date_ok, time_ok FROM w_par_a";
$result6211=mysql_query($query6211);

while ($row6211 = mysql_fetch_array($result6211)) { 

    if ($rows5211[$y5211]['parametername'] == $row6211['parametername']){
       $k1 = $row6211['date_ok'];
        $k2 = $row6211['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row6211['modifyav'];
    $rows6211[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]
    }
    });
    Highcharts.chart('container-chartn11<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
        xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%A', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },
        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows6211, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result6211);

$rows6211="";
$data2="";
}

  mysql_free_result($result5211);

   
 

mysql_close($link5211);



 

?>



<?php 

                 $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link5411=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link5411);

$query5411 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result5411=mysql_query($query5411);

while ($row5411 = mysql_fetch_assoc($result5411)) { 
    $rows5411[] = $row5411;
} 
 
for($y5411=0;$y5411<count($rows5411);$y5411++)
{
$tam = $rows5411[$y5411]['parametername'] ;
$s = $rows5411[$y5411]['id'] ;

?>


<?php 
 

$query6411 ="SELECT modifyav, parametername, date_ok, time_ok FROM y_par_a";
$result6411=mysql_query($query6411);

while ($row6411 = mysql_fetch_array($result6411)) { 

    if ($rows5411[$y5411]['parametername'] == $row6411['parametername']){
        $k1 = $row6411['date_ok'];
        $k2 = $row6411['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row6411['modifyav'];
    $rows6411[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]
    }
    });
    Highcharts.chart('container-charty11<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
        xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%B', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows6411, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result6411);

$rows6411="";
$data2="";
}

  mysql_free_result($result5411);

   
 

mysql_close($link5411);


?>



<?php 

                 $server_IP = "localhost";
$login = "root";
$password = "";
$database = "u0437274_moseco";
$link7711=mysql_connect($server_IP,$login,$password) or die("Not connected");
mysql_query("SET NAMES utf8mb4");
mysql_select_db($database, $link7711);

$query7711 = "SELECT * FROM wp_parameters WHERE otobr = 'Отображать'";
$result7711=mysql_query($query7711);

while ($row7711 = mysql_fetch_assoc($result7711)) { 
    $rows7711[] = $row7711;
} 
 
for($y7711=0;$y7711<count($rows7711);$y7711++)
{
$tam = $rows7711[$y7711]['parametername'] ;
$s = $rows7711[$y7711]['id'] ;

?>


<?php 
 

$query7811 ="SELECT modifyav, parametername, date_ok, time_ok FROM m_par_a";
$result7811=mysql_query($query7811);

while ($row7811 = mysql_fetch_array($result7811)) { 

    if ($rows7711[$y7711]['parametername'] == $row7811['parametername']){
     $k1 = $row7811['date_ok'];
        $k2 = $row7811['time_ok'];

    $dd = date('Y-m-d H:i:s', strtotime("$k1 $k2"));

    $d = strtotime($dd) *1000;
        $data2 = $row7811['modifyav'];
    $rows7811[] = "[$d,$data2 ]";
      }
 }

 
         ?>

<script>

    Highcharts.setOptions({
        colors: ['#2c82cb', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa', '#e3f0fa','#e3f0fa'],
            lang: {
        months: [
            'Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август',
            'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        weekdays: [
            'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Суббота'
        ]}
    });
    Highcharts.chart('container-chartm11<?php echo $s?>', {
        chart: {
            type: 'area',
            colorCount:10
        },
        title: {
            text: ' '
        },
        yAxis: {
            title: {
                text: ' '
            }
        },
        xAxis: {
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000,
                tickWidth: 0,
                labels: {
                    align: 'center',
                    formatter: function () {
                        return Highcharts.dateFormat('%d.%m', this.value);
                    }
                },
            },
        tooltip: {
            pointFormat: "Значение: {point.y:.4f}"
        },

        plotOptions: {
            colors: ['#e3f0fa'],
            area: {
                fillColor: {

                    linearGradient: {
                        x1:0,
                        y1:0,
                        x2:0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },

        series: [{
            showInNavigator: true,
            lineWidth: 0,
            name: ' ',
            data: [<?php echo join($rows7811, ',') ?>]
        }]
    });
</script>
<?php 
 mysql_free_result($result7811);

$rows7811="";
$data2="";
}

  mysql_free_result($result7711);

   
 

mysql_close($link7711);


?>

<?php
get_footer();