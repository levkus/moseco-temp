<html lang="en"><head>
    <meta charset="utf8mb4">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/fonts.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/style.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/media.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/font/css/font-awesome.min.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/new_style.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/new_media.css">
    <link rel="stylesheet" href="/wp-content/themes/moseco/css/slick.css">
     <link href="/wp-content/themes/moseco/favicon.ico" rel="shortcut icon" type="image/x-icon"> 
     <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet"> 
    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body>



