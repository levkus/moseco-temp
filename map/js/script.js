'use strict';

var map = void 0,
    markers = [];

function getIcon (url, hover, isGrey) {
	if (!hover) {
		var markerImage = new google.maps.MarkerImage(
		  url,
		  new google.maps.Size(60, 60),
		  isGrey? new google.maps.Point(5, 7) : new google.maps.Point(11, 12),
		  new google.maps.Point(0, 0)
		);
	} else {
		var markerImage = new google.maps.MarkerImage(
		  url,
		  new google.maps.Size(60, 60),
		  new google.maps.Point(15, 16),
		  new google.maps.Point(0, 0)
		);
	}
	
	return markerImage;
}	


$(document).ready(function () {
  $('._close_pop').click(function () {
    $('.popup').removeClass('_visible');
  });
  $('.s_aside-menu__content').on('click', '.dangerous-class__element:not(.active)', function () {
	  setActiveElement(this);
	  
	  /*
	  var scroll = selected.find('.dangerous-class__element_description');
	  actived.removeClass('active');
	  actived.find('.dangerous-class__description-wrapper').css({ 
		maxHeight: 0,
		padding: 0
	  });
	  selected.addClass('active');
	  selected.find('.dangerous-class__description-wrapper').css({ 
		maxHeight: scroll[0].scrollHeight,
		padding: '21px 0 14px'
	  });
	  */
  });
  //  Обработчики кликов по названию округа
  $('.s_aside-menu__content_item').click(function () {

    //  ID (используем ОКАТО) административного округа должен быть в data-атрибуте
    var regionId = $(this).data('region');
    selectRegion(regionId);
  });
});

/**
 * Раскрывает активные элементы в сайдбаре + выбранный элемент пишет в легенду на карте
 */
function setActiveElement(elem) {
	var actived = $('.dangerous-class__element.active'),
		  selected = $(elem),
		  element_label = selected.find('.dangerous-class__element_symbol').text(),
		  content_h = $('.s_container__content .g_txt__h1 span').first();
		  
	if (actived.length) {
		actived.removeClass('active');
		actived.find('.dangerous-class__element_description').hide();
	}
	selected.addClass('active');
	selected.find('.dangerous-class__element_description').slideDown(300);
  content_h.text(element_label);
  
  // очистим карту
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  setMarkers(map, element_label);
}

function initMap() {
  var uluru = { lat: 55.7522200, lng: 37.6155600 };
  map = new google.maps.Map(document.getElementById('map'), {
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    zoom: 12,
    center: uluru,
    styles: [{ elementType: 'geometry',
      stylers: [{ color: '#092a34' }]
    }, {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{ color: '#000000' }]
    }, {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{ color: '#104c5e' }]

    }, {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{ color: '#104c5e)' }]
    }, {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{ color: '#000000' }]
    }, {
      featureType: "all",
      elementType: "labels",
      stylers: [{ visibility: "off" }]
    }], "elementType": "geometry"
  });
  
  getDataFromDB()
    .then(function(element_name) {
      setMarkers(map, element_name);
    });
  setRegions();
  setGeolocation(map);
}

function getDataFromDB(map) {
  return fetch("http://178.208.145.33/wp-content/themes/moseco/map/elements.php")
    .then(function(response) {
      return response.json();
    }).then(function(classes) {
      var last_idx = Object.keys(classes).length - 1,
          classes_container = $('.s_aside-menu__content'),
          stations_count_array = [],
          active_element;
      
      // получаем массив из количества станций, выпускающих вещества с максимальным уровнем опасности
      classes[last_idx].forEach(function(element) {
        stations_count_array.push(element.stations_count);  
      });

      // вычисляем самое большое число из массива станций
      var max_stations = Math.max.apply(null, stations_count_array);

      // рисуем блоки
      $.each(classes, function(index, classObj) {
        var class_container = $('<div class="s_aside-menu__content_item dangerous-class"></div>'),
            elements_container = $('<div class="dangerous-class__elements"></div>');

        class_container.append( 
          '<div class="dangerous-class__title">' +
            '<span class="dangerous-class__number">' + index + '</span>' +
            '<span class="dangerous-class__label">Класс опасности</span>' +
          '</div>'
        );

        classObj.forEach(function(element) {
          var active = index == last_idx && element.stations_count == max_stations;

          if (active) {
            active_element = element.name;
          }

          elements_container.append(
            '<div class="dangerous-class__element' + (active ? ' active' : '') + '">' +
              '<div class="dangerous-class__element-container">' +
                '<div class="dangerous-class__element_title">' +
                  '<span class="dangerous-class__element_symbol">' + element.name + '</span>' +
                  '<span class="dangerous-class__element_label">' + element.full_name + '</span>' +
                '</div>' +
                '<div class="dangerous-class__element_description"' + (active ? ' style="display:block"' : '') + '>' + element.description + '</div>' +
              '</div>' +
            '</div>'
          );

          $('.s_container__content .g_txt__h1 span').first().text(element.name);
        });

        class_container.append(elements_container);
        // классы выводим в обратном порядке
        classes_container.prepend(class_container);
      });

      return active_element;
    });
}

function setMarkers(map, element) {
  return fetch('http://178.208.145.33/wp-content/themes/moseco/map/stations.php', {
      method: 'post',
      headers: { "Content-type": "application/x-www-form-urlencoded" },
      body: 'element=' + element
    }).then(function(response){
      return response.json();
    }).then(function(data) {
      var able = data.able,
          disable = data.disable,
          iconUrl, label, params = {};

      able.forEach(function(station) {
        var params = {},
			isGrey = false,
            infowindow = new google.maps.InfoWindow({
              content: 
                '<div style="font-weight: 700; font-size: 12px;">' + station.full_name + '</div>' +
                '<div style="color: #666">' + station.address + '</div>', 
            });

        params.infowindow = infowindow;
        params.pdk = station.pdk.toString();
        if (!station.pdk) {
          iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey.png';
          label = '?';
          $.extend(params, { color: 'grey' });
		  isGrey = true;
        } else {
          if (station.pdk <= 0.8) {
            iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/green.png';
            $.extend(params, { color: 'green' });
          } else if (station.pdk > 0.8 && station.pdk < 1) {
            iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/yellow.png';
            $.extend(params, { color: 'yellow' });
          } else if (station.pdk >= 1) {
            iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/red.png';
            $.extend(params, { color: 'red' });
          }

		  var pdk_str = station.pdk.toString();
          label = pdk_str.slice(0, (pdk_str.indexOf(".")) + 2);
        }

        var marker = new google.maps.Marker({
          position: {
            lat: station.latitude,
            lng: station.longitude,
          },
          map: map,
          icon: getIcon(iconUrl, false, isGrey),
          label: label
        });

        markers.push(marker);
        
        addListenersForMarkers(marker, params);
      })

      disable.forEach(function(station) {
        var marker = new google.maps.Marker({
          position: {
            lat: station.latitude,
            lng: station.longitude,
          },
          map: map,
          icon: getIcon('http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey.png', false, true),
          label: '?'
        });

        markers.push(marker);

        //  отображение увеличенной точки при наведении
        marker.addListener('mouseover', function() {
          marker.setIcon(getIcon('http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey_hover.png', true, true));
        });

        //  отображение увеличенной точки при наведении
        marker.addListener('mouseout', function() {
          marker.setIcon(getIcon('http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey.png', false, true));
        });
      });    		
    });
//   //  Сначала получим координаты
//   fetch("http://ecomonitor.mos.ru/ords/moseco_ahd/moseco/askza_json/", {
//   method: 'post',
//   body: JSON.stringify({date: '22.01.2018'})
// }).then(function (response) {
//     return response.json();
//   }).then(function (data) {
// 	for (var i in data) {
// 		
// 	}
//   });
}

/**
 * Навешивает различные обработчики событий на гугловские маркеры на карте 
 */
function addListenersForMarkers(marker, params) {
  var full_label = marker.label;
	//  отображение попапа при клике
	google.maps.event.addListener(marker, 'click', function () {
		showPopup(marker.label);
	});
  
	//  отображение увеличенной точки при наведении
	marker.addListener('mouseover', function() {
    params.infowindow.open(map, marker);
	if (marker.label !== '?') {
		this.set('label', params.pdk);
	}
    var iconUrl, isGrey = false;
    switch(params.color) {
      case 'green': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/green_hover.png';
        break;
      case 'yellow': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/yellow_hover.png';
        break;
      case 'red': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/red_hover.png';
        break;
      default: 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey_hover.png';
		isGrey = true;
    }
    marker.setIcon(getIcon(iconUrl, true, isGrey));
	});
	
	//  отображение уменьшенной точки при  
	marker.addListener('mouseout', function() {
    params.infowindow.close();
    this.set('label', full_label);
    var iconUrl, isGrey = false;
    switch(params.color) {
      case 'green': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/green.png';
        break;
      case 'yellow': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/yellow.png';
        break;
      case 'red': 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/red.png';
        break;
      default: 
        iconUrl = 'http://178.208.145.33/wp-content/themes/moseco/map/img/global/grey.png';
		isGrey = true;
    }
	marker.setIcon(getIcon(iconUrl, false, isGrey));
	});
}

function setGeolocation(map) {
  $('.btn_geo').click(function () {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        map.setCenter(pos);
      }, function () {
        handleLocationError(true, map.getCenter());
      });
    } else {
      handleLocationError(false, map.getCenter());
    }
  });
}

function selectRegion(regionId) {
  map.data.revertStyle();
  map.data.forEach(function (feature) {
    if (feature.getProperty('OKATO') == regionId) {
      feature.setProperty('selected', !feature.getProperty('selected'));
    };
  });
}

function setRegions() {

  //  Загружаем границы административных округов из локального geoJSON'а
  map.data.loadGeoJson('/data/ao.geojson');
  map.data.setStyle(function (feature) {
    var strokeColor = 'rgba(0, 0, 0, 0)';
    var fillColor = 'rgba(0, 0, 0, 0)';
    if (feature.getProperty('selected')) {
      strokeColor = 'rgba(255, 210, 0, 0.66)';
      fillColor = 'rgba(255, 245, 0, 0.1)';
    }
    return {
      fillColor: fillColor,
      strokeColor: strokeColor,
      strokeWeight: 1
    };
  });
  map.data.addListener('click', function (event) {
    var feature = event.feature;
    feature.setProperty('selected', !feature.getProperty('selected'));
  });
}

function showPopup(label) {

  //  Сначала стучимся на сервер, мне кажется оптимальным, если стучаться по url'у в котором есть id метки
  fetch('data/popup_' + label)

  //  fetch возвращает промис
  .then(function (response) {
    return response.json();
  }).then(function (response) {
    var description = response.description,
        data = response.data,
        totalConcentration = response.totalConcentration;

    //  Формируем шапку popup'а

    var popupHeaderBlock = '<div class="popup_head__left">\n<h2 class="g_txt__h3">' + description.title + '</h2>\n<div class="popup_local">\n<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="21" viewBox="0 0 19 21"><defs><path id="xwdva" d="M630.51 200.49V182.5h13.98v17.98zm12.12-8.53a4.8 4.8 0 0 0-.61-4.74 5.48 5.48 0 0 0-4.52-2.22c-1.84 0-3.49.8-4.52 2.22a4.87 4.87 0 0 0-.63 4.74c.17.42.44.85.79 1.24l4.36 4.8 4.34-4.8c.35-.4.64-.8.8-1.24zm-5.13-.16c-1.23 0-2.24-.93-2.24-2.06s1.01-2.06 2.24-2.06c1.23 0 2.24.93 2.24 2.06s-1.01 2.06-2.24 2.06z"/><path id="xwdvb" d="M642.63 191.96c-.15.44-.44.85-.79 1.24l-4.34 4.8-4.36-4.8c-.35-.4-.62-.82-.8-1.24a4.87 4.87 0 0 1 .64-4.74 5.48 5.48 0 0 1 4.52-2.22c1.84 0 3.49.8 4.52 2.22a4.8 4.8 0 0 1 .61 4.74zm-2.9-2.22c0-1.13-1-2.06-2.23-2.06s-2.24.93-2.24 2.06 1.01 2.06 2.24 2.06c1.23 0 2.24-.93 2.24-2.06z"/><clipPath id="xwdvc"><use fill="#fff" xlink:href="#xwdva"/></clipPath></defs><g><g transform="translate(-628 -181)"><use fill="#fff" fill-opacity="0" stroke="#cdcdcd" stroke-miterlimit="50" stroke-width="4" clip-path="url(&quot;#xwdvc&quot;)" xlink:href="#xwdvb"/></g></g></svg>\n<span>' + description.address + '</span>\n</div>\n</div>\n<div class="popup_head__right">\n<div class="ic-blocks active">\n<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="22" viewBox="0 0 22 22"><defs><path id="trvba" d="M1103 188v-8h8v8zm11 0v-8h8v8zm-11 11v-8h8v8zm11 0v-8h8v8z"/><clipPath id="trvbb"><use fill="#fff" xlink:href="#trvba"/></clipPath></defs><g><g opacity="1" transform="translate(-1102 -178)"><use fill="#fff" fill-opacity="0" stroke="#000" stroke-miterlimit="50" stroke-width="4" clip-path="url(&quot;#trvbb&quot;)" xlink:href="#trvba"/></g></g></svg>\n</div>\n<div class="ic-tables">\n<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21" height="17" viewBox="0 0 21 17"><defs><path id="kgz3a" d="M1141 185v-3h4v3zm6 0v-3h13v3zm-6 6v-3h4v3zm6 0v-3h13v3zm-6 6v-3h4v3zm6 0v-3h13v3z"/></defs><g><g transform="translate(-1140 -181)"><use xlink:href="#kgz3a"/></g></g></svg>\n</div>\n</div>';

    $('.popup_head').html(popupHeaderBlock);

    //  Общая концентрация

    var popupTotalConcentration = '<div class="g_txt">\n<h2 class="g_txt__h4">\u041E\u0431\u0449\u0430\u044F \u043A\u043E\u043D\u0446\u0435\u043D\u0442\u0440\u0430\u0446\u0438\u044F</h2>\n<p>\u041D\u043E\u0440\u043C\u0430: <span>' + totalConcentration.norm + '</span></p>\n</div>\n<div class="numb__result ' + totalConcentration.status + '">' + totalConcentration.quantity + '</div>';

    $('.popup_result').html(popupTotalConcentration);
    $('.ic-tables').click(function () {
      $(this).addClass('active');
      $('.ic-blocks').removeClass('active');
      $('.popup_content-2').addClass('visible');
      $('.popup_content').removeClass('visible');
    });
    $('.ic-blocks').click(function () {
      $(this).addClass('active');
      $('.ic-tables').removeClass('active');
      $('.popup_content').addClass('visible');
      $('.popup_content-2').removeClass('visible');
    });

    //  Проходимся по массиву и формируем html popup'а
    var popupBlocks = '';
    var popupTable = '';
    data.forEach(function (item) {
      popupBlocks += '<div class="popup__block">\n<div class="popup_content__substance">' + item.substance + '</div>\n<div class="popup_content__info">\n<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="16" height="16"><style>.a{fill:#7f7f7f;}</style><path d="M256 0C114.5 0 0 114.5 0 256c0 141.5 114.5 256 256 256 141.5 0 256-114.5 256-256C512 114.5 397.5 0 256 0zM256 472c-119.4 0-216-96.6-216-216 0-119.4 96.6-216 216-216 119.4 0 216 96.6 216 216C472 375.4 375.4 472 256 472z" class="a"/><path d="M256 128.9c-11 0-20 9-20 20V277.7c0 11 9 20 20 20s20-9 20-20V148.9C276 137.8 267 128.9 256 128.9z" class="a"/><circle cx="256" cy="349.2" r="27" class="a"/></svg>\n</div>\n<div class="popup_content__name">' + item.name + '</div>\n<div class="numb__item ' + item.level + '">' + item.numb + '</div>\n<div class="popup_content__indication">' + item.indication + '</div>\n<div class="popup_content__class">' + item.class + '</div>\n</div>';
      popupTable += '<tr>\n<td>\n<span class="popup_content__info">\n<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="16" height="16"><style>.a{fill:#7f7f7f;}</style><path d="M256 0C114.5 0 0 114.5 0 256c0 141.5 114.5 256 256 256 141.5 0 256-114.5 256-256C512 114.5 397.5 0 256 0zM256 472c-119.4 0-216-96.6-216-216 0-119.4 96.6-216 216-216 119.4 0 216 96.6 216 216C472 375.4 375.4 472 256 472z" class="a"/><path d="M256 128.9c-11 0-20 9-20 20V277.7c0 11 9 20 20 20s20-9 20-20V148.9C276 137.8 267 128.9 256 128.9z" class="a"/><circle cx="256" cy="349.2" r="27" class="a"/></svg>\n</span>\n<span class="popup_content__substance">' + item.substance + '</span>\n<span class="popup_content__name">' + item.name + '</span>\n</td>\n<td><span class="popup_content__class">' + item.class + '</span></td>\n<td><span class="popup_content__indication">' + item.indication + '</span></td>\n<td>\n<span class="numb__item ' + item.level + '">' + item.numb + '</span>\n</td>\n</tr>';
    });

    //  Собираем и выводим popup
    $('.popup_content').html(popupBlocks);
    $('#popupTable').html(popupTable);
    $('.popup').addClass('_visible');
  });
}
