<?php
/*
Template Name: СМИ
*/

get_header(); ?>

	<section class="sidebar-block">
    <div class="logo">
        <a href="<?php echo get_home_url(); ?>"><?php the_field('name_site','option'); ?></a>
      
    </div>
    <div class="list-menu">
        <?php wp_nav_menu( array(
    'theme_location'  => 'menu-1',
    
) ); ?>
<li class="last-interactive-map"><a href="/karta/">Интерактивная карта</a></li>
    </div>
    <div class="social-network">
        <span>Мы в соцсетях</span>
        <div class="social-links">
             <?php if( have_rows('soc', 'option') ): ?>
    <?php while( have_rows('soc', 'option') ): the_row(); 
        ?>
         <a href="<?php the_sub_field('link_soc', 'option')?>" target="_blank" style="background: url('<?php the_sub_field('im_soc', 'option')?>') center no-repeat;"></a>
    <?php endwhile; ?>
<?php endif; ?>
        </div>
    </div>
</section>


<section class="main-content">
    <div class="full-height-content chart-header" style="background: url('<?php the_field('image'); ?>') top/cover no-repeat;">

        <div class="content-part ">
            <div class="main-top-menu">
                 <?php wp_nav_menu( array(
    'theme_location'  => 'menu-2',
) ); ?>
                <!--div class="callback-block">
                    <a href="#" target="_blank">Обратная связь</a>
                </div>
               < <div class="search">
                    <form action="">
                        <input type="text" placeholder="Поиск по порталу">
                        <input type="submit">
                    </form>
                </div>-->
            </div>
            <div class="wrap-content">
                <div class="left-inner-content">
                    <div class="big-title ">
                        <h1><?php the_field('name'); ?></h1>
                    </div>
                    <div class="doklad-za-god">
                       <a href="<?php the_field('dd');?>" target="_blank"> <img src="<?php echo get_template_directory_uri();?>/img/pdf_big.png" alt="">
                        <span>Доклад за 2016 год</span></a>
                    </div>
                    <div class="sub-doklad">
                        Динамика выбросов загрязняющих </br>веществ по округам г. Москвы, тонн
                    </div>
                    <div class="cahrt-header-column">
                        <!--div class="tab-date">
                            <span class="active">Воздух </span>
                            <span>Вода</span>
                        
                            <span>Почва</span>
                        </div-->
                        <!--div id="container-chart" style="min-width: 310px; height: 200px; margin: 0 auto"></div-->
                        <div class="graph-for-smi">
                            <img class="graph-for-smi-img" src="<?php echo get_template_directory_uri();?>/img/graph.png">
                        </div>
                        <!--div class="load-more red-bottom">
                            <a href="#" target="_blank">Посмотреть весь доклад за 2016</a>
                        </div-->
                    </div>

                </div>
                <div class="about-slider-content small">
                    <div class="overlay-title"><h2>Доклады прошлых лет</h2></div>
                    <?php if( have_rows('doc') ): ?>
	<?php while( have_rows('doc') ): the_row(); 
		?>
<a class="doklad-link" href="<?php the_sub_field('docl');?>" target="_blank"><div class="doklad-block">
                        <?php the_sub_field('name');?>
                    </div></a>
	<?php endwhile; ?>
<?php endif; ?>
                    
                 
                </div>
            </div>
        </div>
    </div>
    <div class="air-content">
        <div class="bold-title">
            <h2>Новости</h2>
        </div>
        <div class="news-content">
            <div class="last-news">
                <?php
global $post;
$args = array( 'numberposts' => 1 , 'category' => 2, 'orderby' => 'date');
$myposts = get_posts( $args );
foreach( $myposts as $post ){ setup_postdata($post);
?>
<div class="img-new">
                    <img src="<?php the_field('img'); ?>" alt="">
                </div>
                <div class="news-text">
                    <div class="category"><?php the_category(', ') ?></div>
                    <h2 class="title-news"><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></h2>
                    <div class="short-text"><?php the_content(); ?></div>
                    <div class="posted-date-news"><?php the_date(); ?></div>
                </div>
<?php
}
wp_reset_postdata();
?>
                
            </div>
            <div class="block-news-content">

                <?php
if ( have_posts() ) :
  query_posts('cat=2'); 
  while (have_posts()) : the_post(); 
?>
<div class="news-item lm2">
                    <div class="img-new">
                        <div class="news-item-image" style="background-image: url(<?php the_field('img'); ?>)"></div>
                    </div>
                    <div class="news-text">
                        <div class="category"><?php the_category(', ') ?></div>
                        <h2 class="title-news"><a href="#" target="_blank"><?php the_title(); ?></a></h2>
                        <div class="posted-date-news"><?php the_date(); ?></div>
                    </div>
                </div>
<?php  
endwhile; 
endif;
wp_reset_query();                
?>
                
              

                
            </div>
            <div class="load-more">
                <a href="#" target="_blank" id="loadMore2">Посмотреть все</a>
            </div>
        </div>



        <div class="pdf-content blue-bg envelope-block">
            <div class="text-vacancy">
                <div class="vac-tit">Эконовости</div>
                <div class="text-content"><?php the_field('pod'); ?></div>
                 <?php echo do_shortcode(' [contact-form-7 id="287" title="Контактная форма 1"]'); ?>
              
            </div>
        </div>

        <div class="video-content-wrapper">
            <div class="bold-title">
                <h2>Видео материалы</h2>
            </div>
            <div class="video-teaser-wrapper">
            <?php if( have_rows('video') ): ?>
            <?php while( have_rows('video') ): the_row();
            ?>


                <div class="video-teaser" style="background:linear-gradient(to top, rgba(9, 0, 3, 0.8), rgba(146, 156, 163, 0)), url(<?php the_sub_field('im_v')?>) no-repeat 0 0;">

                    <div class="video-teaser-desc">
                        <div class="video-teaser-a">
                            <img src="<?php the_sub_field('im_p')?>" class="video-teaser-desc__img">
                            <span class="video-teaser-desc__author">
                            <?php the_sub_field('fio')?>
                        </span>
                        </div>
                        <h1 class="video-teaser-desc__title">
                            <?php the_sub_field('name')?>
                        </h1>

                        <div class="video-teaser-footer">
                        <span class="video-teaser-footer__date">
                            <?php the_sub_field('date')?>
                        </span>
                            <span class="video-teaser-footer__time">
                            <?php the_sub_field('prodol')?>
                        </span>
                        </div>
                    </div>

                    <a target="_blank" href="<?php the_sub_field('link_v')?>" class="video-teaser__link"></a>

                </div>




        <?php endwhile; ?>
        <?php endif; ?>
            <div class="load-more">
                <a href="#" target="_blank" id="loadMoreVideo">Посмотреть все</a>
            </div>
        </div>
        <!--div class="video-content">
            <?php if( have_rows('video') ): ?>
    <?php while( have_rows('video') ): the_row();
        ?>
         <div class="col-6 lm">
                <div class="video-block">
                    <div class="vid-img">
                        <img src="<?php the_sub_field('im_v')?>" alt="">
                    </div>
                    <a href="<?php the_sub_field('link_v')?>" target="_blank" class="play-button" >
                        <div></div>
                    </a>
                    <div class="texts-video">
                        <div class="posted-by">
                            <div class="pos-by-img">
                                <img src="<?php the_sub_field('im_p')?>" alt="">
                            </div>
                           <?php the_sub_field('fio')?>
                        </div>
                        <div class="title-video">
                            <?php the_sub_field('name')?>
                        </div>
                        <div class="posted-video">
                            <span class="pos-date"><?php the_sub_field('date')?></span>
                            <span class="how-last"><?php the_sub_field('prodol')?></span>
                        </div>
                    </div>
                </div>
            </div>
    <?php endwhile; ?>
<?php endif; ?>


            <div class="load-more">
                <a href="#" target="_blank" id="loadMore">Посмотреть все</a>
            </div>
        </div -->

        <div class="bold-title">
            <h2>Статьи</h2>
        </div>
        <div class="service-items-type in-smi-page">
            <?php if( have_rows('stat') ): ?>
    <?php while( have_rows('stat') ): the_row(); 
        ?>
         <div class="items-type col-4">
                <div class="it-ty-img">
                    <img src="<?php echo get_template_directory_uri();?>/img/pdf_blue.png" alt="">
                </div>
                <a href="<?php the_sub_field('link_s')?>" target="_blank"><?php the_sub_field('name_s')?></a>
                <span><?php the_sub_field('opis_s')?></span>
            </div>
    <?php endwhile; ?>
<?php endif; ?>
            
            
        </div>

        <div class="footer">
            <div class="row">
                <div class="col-6 right-footer">
                    <div class="footer-tit">Контакты</div>
                    <div class="footer-contacts">
                        <div><span>Тел.:</span><?php the_field('phone','option'); ?></div>
                        <div><span>Факс:</span><?php the_field('fax','option'); ?></div>
                        <div><span>E-mail:</span><?php the_field('e-mail','option'); ?></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="visually-block">
                       
                        <a href="<?php the_field('map','option'); ?>" class="map-site">Карта сайта</a>
                    </div>
                    <div class="copyright">
                       <?php the_field('copy','option'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

	
<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/highchart.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/slick.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/common.js" type="text/javascript"></script>
<script>

    Highcharts.chart('container-chart', {
        chart: {
            type: 'column',
            backgroundColor:'rgba(255, 255, 255, 0.0)'
        },
        title: {
            text: ' '
        },
        xAxis: {
            categories: [
                'ВАО',
                'ЗАО',
                'ЗЕЛАО',
                'CAО',
                'CВАО',
                'CЗАО',
                'ТИНАО',
                'ЦАО',
                'ЮАО',
                'ЮВАО',
                'ЮЗАО'
            ],
            crosshair: true,
            labels: {
                style: {
                    color: '#fff'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ' '
            },
            gridLineColor: 'rgba(255, 255, 255, 0.2)',
            labels: {
                style: {
                    color: '#fff'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {

            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },



        }
        ,
        legend: {
            itemStyle: {
                color: '#fff',
                fontWeight: 'normal',

            },
            symbolHeight: 12,
            symbolWidth: 12,
            symbolRadius: 0,
            itemHoverStyle: {
                color: '#ffd200'
            },
            align: 'right',
            verticalAlign: 'top'
        },

        series: [{
            name: '2014',
            data: [1, 0.8, 1, 2, 1, 0.8, 1, 1.5, 2, 1.1, 2],
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 2.5 },
                stops: [
                    [0, '#ffd200'],
                    [1, 'transparent']
                ]
            }
            }, {
            name: '2015',
            data: [1.5, 2, 2, 2, 1, 0.9, 1, 1.5, 2, 1.5, 1.5],
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 2.5 },
                stops: [
                    [0, '#67d000'],
                    [1, 'transparent']
                ]
            }

            }, {
            name: '2016',
            data: [1.5, 0.8, 1, 1.5, 2, 2, 2, 1.5, 2.5, 1, 2],
            color: {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 2.5 },
                stops: [
                    [0, '#6fb3e4'],
                    [1, 'transparent']
                ]
            }
        }]
    });
</script>
<?php
get_footer();